import json  
import os
import requests
import uuid
from flask import flash, Flask, jsonify, render_template, redirect, request, send_from_directory, url_for
from werkzeug.utils import secure_filename

app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = 'files'
app.secret_key = os.urandom(24)

ALLOWED_EXTENSIONS = set(['pdf', 'png', 'jpg'])
APP_ROOT = os.path.dirname(os.path.abspath(__file__))

def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

@app.route('/show/<filename>')
def uploaded_file(filename):
    filename = 'http://127.0.0.1:80/uploads/' + filename
    return render_template('template.html', filename=filename)

@app.route('/uploads/<filename>')
def send_file(filename):
    x = send_from_directory(app.config['UPLOAD_FOLDER'], filename)
    print x
    return send_from_directory(app.config['UPLOAD_FOLDER'], filename)

@app.route('/', methods=['GET', 'POST'])
def upload_file():
    if request.method == 'POST':
        if 'file' not in request.files:
            flash('No file part')
            return redirect(request.url)
        file = request.files['file']

        if file.filename == '':
            flash('No selected file')
            return redirect(request.url)
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            path = os.path.join(APP_ROOT, app.config['UPLOAD_FOLDER'], filename)
            file.save(path)
            files = {'file': open(path,'rb')}
            values = {'img_id': str(uuid.uuid4())}
            r = requests.post('http://ocr-service:8080/', files=files, data=values)
            result = r.content
            print(result)
            obj = json.loads(result)  

            print(obj)
            return render_template('template.html', 
                ocr_text=obj['ocr'], 
                name_entities=obj['nameEntities'],
                date_values=obj['dates'],
                amout_values=obj['amount'],
                lang=obj['lang'])
        else:
            flash('File type not supported!')
            return redirect(request.url)
    return render_template('template.html')

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=80)
