## Usage

1. Build the app
```bash
$ docker-compose build
$ docker-compose up
```

2. Bring the app up
```bash
$ docker-compose up
```

3. Browse to localhost:3000 to see the app in action.

## Notes
1. Supported File types are:
    - jpg
    - png
    - pdf
    
2. Supported Date type:
    - xxxx-xx-xx
    - xx-xx-xxxx
    - xx/xx/xxxx
    - xx/xx/xxxx

3. Supported Amount type:
    - Number with dot as the decimal separator
    - Number with comma as the thousand separator and the dot as the decimal separator