import logging, json, os, pika, re, time
from time import sleep

QUEUE = os.environ.get('DATE_AMOUNT_CALLBACK_QUEUE')
RABBIT_MQ_USER = os.environ.get('RABBITMQ_USER')
RABBIT_MQ_PASS = os.environ.get('RABBITMQ_PASS')

def on_request(ch, method, props, body):
    print method.delivery_tag
    print body
    body_json = json.loads(body)             
    data = body_json['data']
    dates = re.findall(r"(?:\d{1,2}(?:\/|-)\d{1,2}(?:\/|-)\d{4})|(?:\d{4}(?:\/|-)\d{1,2}(?:\/|-)\d{1,2})", data)
    amount = re.findall(r"(?:[0-9]{1,3}(?:,[0-9]{3})*\.[0-9]+)|(?:\d+\.[0-9]+)", data)
    matches = {'dates': dates, 'amount': amount}
    print(matches)
    ch.basic_publish(
        exchange='',
        routing_key=props.reply_to,
        properties=pika.BasicProperties(correlation_id = props.correlation_id),
        body=json.dumps({'dateAndAmount': matches}))
    ch.basic_ack(delivery_tag=method.delivery_tag)

if __name__ == "__main__":
    sleep(10)
    credentials = pika.PlainCredentials(RABBIT_MQ_USER, RABBIT_MQ_PASS)
    parameters = pika.ConnectionParameters('rabbitmq-server', 5672, '/', credentials)
    connection = pika.BlockingConnection(parameters)
    channel = connection.channel()
    channel.queue_declare(queue=QUEUE)
    channel.basic_qos(prefetch_count=1)
    channel.basic_consume(queue=QUEUE, on_message_callback=on_request)
    print("Awaiting Name entity requests")
    try:
        channel.start_consuming()
    except KeyboardInterrupt:
        channel.stop_consuming()
        connection.close()