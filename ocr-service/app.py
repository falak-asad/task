import json, multiprocessing, os, pytesseract, re, tempfile
from flask import Flask, jsonify, request
from multiprocessing import Process
from pdf2image import convert_from_path
from PIL import Image
from pytesseract import image_to_string 
from services.queue_client import QueueClient

app = Flask(__name__)

DATE_AMOUNT_CALLBACK_QUEUE = os.environ.get('DATE_AMOUNT_CALLBACK_QUEUE')
NAME_ENTITY_CALLBACK_QUEUE = os.environ.get('NAME_ENTITY_CALLBACK_QUEUE')
LANG_CALLBACK_QUEUE = os.environ.get('LANG_CALLBACK_QUEUE')

def enqueueData(img_id, ocrText, queue, result, key):
    print('img_id', img_id)
    print('queue', queue)
    print("Requesting " + key)    
    qc = QueueClient()
    obj = qc.call(img_id, queue, {'data': ocrText})
    obj = json.loads(obj) 
    print("Result for " + key)
    print (obj)
    result[key] = obj[key]

@app.route('/', methods=['POST'])
def ocr():
    file = request.files['file']
    img_id = request.form['img_id']
    print("file", file)
    tempDir = tempfile.mkdtemp()
    filename = file.filename
    filePath = os.path.join(tempDir, filename)
    file.save(filePath)    
    fileBaseName, fileExtension = os.path.splitext(filePath)
    files = []
    if (fileExtension == '.pdf'):
        images_from_path = convert_from_path(
            filePath, 
            output_folder=tempDir,  
            first_page =0)
        i = 0
        for page in images_from_path:
            i += 1
            newFilePath = os.path.join(tempDir, fileBaseName + str(i) + '.jpg')
            page.save(newFilePath, 'JPEG')
            files.append(newFilePath)
    else:
        files.append(filePath)

    ocrText = ''
    for filename in files:
        if filename.endswith(".jpg") or filename.endswith(".png"): 
            ocrText += image_to_string(
                Image.open(os.path.join(tempDir, filename))) + "\n"

    # Starting services in parallel
    manager = multiprocessing.Manager()
    result = manager.dict()
    result['nameEntities'] = []
    result['dateAndAmount'] = {
        'dates': [],
        'amount': []
    }
    result['lang'] = ''
    p1 = Process(
        target=enqueueData, 
        args=(img_id, ocrText, NAME_ENTITY_CALLBACK_QUEUE, result, 'nameEntities',))
    p1.start()
    p2 = Process(
        target=enqueueData, 
        args=(img_id, ocrText, DATE_AMOUNT_CALLBACK_QUEUE, result, 'dateAndAmount', ))    
    p2.start()
    p3 = Process(
        target=enqueueData, 
        args=(img_id, ocrText, LANG_CALLBACK_QUEUE, result, 'lang',))    
    p3.start()
    p1.join()
    p2.join()
    p3.join()
    print(result)
    return jsonify({
        'img_id': img_id, 
        'ocr': ocrText,
        'nameEntities': result['nameEntities'],
        'dates': result['dateAndAmount']['dates'],
        'amount': result['dateAndAmount']['amount'],
        'lang': result['lang']
    }), 201

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=8080)

