import json, logging, os, pika, spacy, time
from time import sleep

QUEUE = os.environ.get('NAME_ENTITY_CALLBACK_QUEUE')
RABBIT_MQ_USER = os.environ.get('RABBITMQ_USER')
RABBIT_MQ_PASS = os.environ.get('RABBITMQ_PASS')

def on_request(ch, method, props, body):
    print method.delivery_tag
    print body
    nlp = spacy.load("en_core_web_sm")
    body_json = json.loads(body)             
    doc = nlp(body_json['data'])
    response = []
    for ent in doc.ents:
        response.append(ent.text + '=>' + ent.label_)
    ch.basic_publish(
        exchange='',
        routing_key=props.reply_to,
        properties=pika.BasicProperties(correlation_id = props.correlation_id),
        body=json.dumps({'nameEntities': response}))
    ch.basic_ack(delivery_tag=method.delivery_tag)

if __name__ == "__main__":
    sleep(10)
    credentials = pika.PlainCredentials(RABBIT_MQ_USER, RABBIT_MQ_PASS)
    parameters = pika.ConnectionParameters('rabbitmq-server', 5672, '/', credentials)
    connection = pika.BlockingConnection(parameters)
    channel = connection.channel()
    channel.queue_declare(queue=QUEUE)
    channel.basic_qos(prefetch_count=1)
    channel.basic_consume(queue=QUEUE, on_message_callback=on_request)
    print("Awaiting Name entity requests")    
    try:
        channel.start_consuming()
    except KeyboardInterrupt:
        channel.stop_consuming()
        connection.close()